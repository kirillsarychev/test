<?php
namespace app\components\checkContent\interfaces;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Kirill Sarychev
 */
interface checkContentInterface {
    
public function checkContent(string $string);

public function setError($err);

public function getErrors();
}
