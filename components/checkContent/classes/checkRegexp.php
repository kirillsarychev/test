<?php
namespace app\components\checkContent\classes; 
use app\components\checkContent\interfaces;

/**
 * Check containce one of stop words in string
 *
 * @author Kirill Sarychev
 */
class checkRegexp implements interfaces\checkContentInterface {
    
    /**
     * @var array containce all errors
     */
    private $errors=[];
    
    /**
     * @var array containce all stop words collection
     */
    private $stopWordsCollection=[
        'SiteAnalyst',
	'SiteAdmin',
	'Administration',
    ];
    
    public function checkContent(string $string){
        foreach ($this->stopWordsCollection as $key => $stopWord){
            if ($stopWord == $string) {
                $this->setError($string);
            }
        }
    }
    public function setError($err){
        $this->errors[]=$err;
    }
    public function getErrors(): array {
        return $this->errors;
    }
}
