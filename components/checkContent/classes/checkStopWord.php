<?php
namespace app\components\checkContent\classes; 
use app\components\checkContent\interfaces;

/**
 * Check containce needle regexp in string
 *
 * @author Kirill Sarychev
 */
class checkStopWord implements interfaces\checkContentInterface {
    
    /**
     * @var array containce all errors
     */
    private $errors=[];
    
    /**
     * @var array containce all regexp collection
     */
    private $regexpCollection=[
        '(f+a+c+e+b+[o0]+[o0]+k+)',
	'(f+(a+)?.{1,2}c+e+b+.{1,2}o+k+)',
	'(f+.{1,3}b+o+[kc]+)',
    ];
    
    public function checkContent(string $string){
        foreach ($this->regexpCollection as $key => $regExp){
            if (preg_match($regExp, $string)) {
                $this->setError($string);
                break;
            }
        }
    }
    public function setError($err){
        $this->errors[]=$err;
    }
    public function getErrors(): array {
        return $this->errors;
    }
}
