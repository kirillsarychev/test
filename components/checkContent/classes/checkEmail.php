<?php
namespace app\components\checkContent\classes; 
use app\components\checkContent\interfaces;

/**
 * Class for checking emeils string 
 *
 * @author Kirill Sarychev
 */
class checkEmail implements interfaces\checkContentInterface {
    
    /**
     *
     * @var array containce all errors
     */
    private $errors=[];
    
    public function checkContent(string $string){
        if (filter_var($string, FILTER_VALIDATE_EMAIL)) {
            $this->setError($string);
        }
    }
    public function setError($err){
        $this->errors[]=$err;
    }
    public function getErrors(): array {
        return $this->errors;
    }
}
