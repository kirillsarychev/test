<?php

namespace app\components\checkContent;
 
use yii\base\Component;
use app\components\checkContent\classes\checkEmail;
use app\components\checkContent\classes\checkUrl;
use app\components\checkContent\classes\checkRegexp;
use app\components\checkContent\classes\checkStopWord;

/**
 * Description of checkContentComponent
 *
 * @author Kirill Sarychev
 */
class CheckContentComponent extends Component{
    private $filterClasses = [
        checkEmail::class,
        checkUrl::class,
        checkStopWord::class,
        checkRegexp::class,
    ];
    public $filters=[];
    public $errors=[];
    
     public function init(){
         foreach ($this->filterClasses as $class){
             $this->filters[] = new $class;
         }
         parent::init();
    }
    
    public function checkString(string $string){
        $words = preg_split("/[\s,]+/",$string);
        foreach($words as $word) {
            $this->checkError($word);
        }
        return $this->getContentErrors();
    }
    
    private function getContentErrors(){
        foreach ($this->filters as $filter){
            $this->errors[get_class($filter)]=$filter->getErrors();
        }
        return $this->errors??false;
    }
        private function checkError(string $word){
        foreach ($this->filters as $filter){
           $filter->checkContent($word);
        }
    }
}
