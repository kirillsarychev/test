<?php
namespace TestCheckContent;

class RegexpTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $validationClass;
    protected $testString;

    protected function _before()
    {
        $this->validationClass = new classes\checkRegexp();
    }
        protected function _after()
    {
            return true;
    }

    public function testString()
    {
        $this->testString='asdas   facebook  dsdasdas';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue($this->validationClass->getErrors());
    }
    public function testEmpty()
    {
        $this->testString='';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
        public function testWhithAnotherSymbols()
    {
        $this->testString='asdas   facebooksdsd  dsdasdas';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue($this->validationClass->getErrors());
    }
        public function testCaseSensitive()
    {
        $this->testString='test FacEBook  test';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
    public function testInt()
    {
        $this->testString=123;
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
}