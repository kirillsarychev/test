<?php
namespace TestCheckContent;
use app\components\checkContent\classes;

class EmailTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $validationClass;
    protected $testString;

    protected function _before()
    {
        $this->validationClass = new classes\checkEmail;
    }
        protected function _after()
    {
            return true;
    }

    public function testEmail()
    {
        $this->testString='test@test.test';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue($this->validationClass->getErrors());
    }
    public function testEmptyMail()
    {
        $this->testString='';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }

        public function testMailWhithRussanSymbols()
    {
        $this->testString='test@тест.test';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
        public function testNoMail()
    {
        $this->testString='testтест.test';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
    public function testInt()
    {
        $this->testString=123;
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
}