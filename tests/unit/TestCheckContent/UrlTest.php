<?php
namespace TestCheckContent;

class UrlTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
 protected $validationClass;
    protected $testString;

    protected function _before()
    {
        $this->validationClass = new classes\checkEmail;
    }
        protected function _after()
    {
            return true;
    }

    public function testUrl()
    {
        $this->testString='https://google.com.ua';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue($this->validationClass->getErrors());
    }
    public function testMailTo()
    {
        $this->testString='mailto:test@test.test';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue($this->validationClass->getErrors());
    }

        public function testEmpty()
    {
        $this->testString='';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
        public function testWhithRussanSymbols()
    {
        $this->testString='http://тест.укр';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
    public function testInt()
    {
        $this->testString=123;
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
}