<?php
namespace TestCheckContent;

class StopWordTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
     protected $validationClass;
    protected $testString;

    protected function _before()
    {
        $this->validationClass = new classes\checkEmail;
    }
        protected function _after()
    {
            return true;
    }

    public function testStopWord()
    {
        $this->testString='SiteAnalyst';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue($this->validationClass->getErrors());
    }
    public function testEmpty()
    {
        $this->testString='';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
        public function testWhithRussanSymbols()
    {
        $this->testString='SiteАnаlyst';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
        public function testCaseSensitive()
    {
        $this->testString='Siteanalyst';
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
    public function testInt()
    {
        $this->testString=123;
        $this->validationClass->checkContent($this->testString);
        $this->assertTrue(!$this->validationClass->getErrors());
    }
}